﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;
using RssReader.Model;
using RssReader.ViewModel;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace RssReader
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage
    {
        private readonly MainViewModel _viewModel;
        public MainPage()
        {
            InitializeComponent();
            _viewModel = new MainViewModel { IsProgressRingVisible = true };
        }

        private void ArticlesListView_OnTapped(object sender, TappedRoutedEventArgs e)
        {
            _viewModel.IsWebViewVisible = false;
            _viewModel.IsDetailsTabVisible = true;
            _viewModel.SelectedArticle = ArticlesListView.SelectedItem as Article;
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            _viewModel.IsWebViewVisible = true;
            AdditionalWebView.Navigate(new Uri(_viewModel.SelectedArticle.ReadMore));
        }

        private async void MainPage_OnLoaded(object sender, RoutedEventArgs e)
        {
            var content = await _viewModel.GetRssContent();
            _viewModel.GetContentData(content);
            DataContext = _viewModel;
            _viewModel.IsProgressRingVisible = false;
        }
    }
}
