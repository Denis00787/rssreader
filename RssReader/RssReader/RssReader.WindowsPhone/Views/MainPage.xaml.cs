﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;
using RssReader.Model;
using RssReader.ViewModel;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace RssReader
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage
    {
        private readonly MainViewModel _viewModel;
        public MainPage()
        {
            InitializeComponent();
            _viewModel = new MainViewModel();
            NavigationCacheMode = NavigationCacheMode.Required;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }

        private async void MainPage_OnLoaded(object sender, RoutedEventArgs e)
        {
            var content = await _viewModel.GetRssContent();
            _viewModel.GetContentData(content);
            DataContext = _viewModel;
        }

        private void ArticlesListView_OnTapped(object sender, TappedRoutedEventArgs e)
        {
            _viewModel.SelectedArticle = ArticlesListView.SelectedItem as Article;
            Frame.Navigate(typeof(ArticleDetailsPage),_viewModel);
        }
    }
}
