﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using RssReader.Model;

namespace RssReader.ViewModel
{
    public class MainViewModel : INotifyPropertyChanged
    {
        #region Variables
        private const string _rssPath = "http://techcrunch.com/feed/";
        private ObservableCollection<Article> _artiles;
        public event PropertyChangedEventHandler PropertyChanged;
        private const string _imageRegexPath = "<img.+?src=[\"'](.+?)[\"'].+?>";
        private const string _readMeRegexPath = "<a.+?href=[\"'](.+?)[\"'].+?>";
        #endregion

        #region Properties
        public ObservableCollection<Article> Articles
        {
            get { return _artiles; }
            set
            {
                if (!Equals(_artiles, value))
                {
                    _artiles = value;
                }
            }
        }

        private bool _isDetailsTabVisible;
        public bool IsDetailsTabVisible
        {
            get { return _isDetailsTabVisible; }
            set
            {
                _isDetailsTabVisible = value;
                OnPropertyChanged("IsDetailsTabVisible");
            }
        }

        private Article _selectedArticle;
        public Article SelectedArticle
        {
            get { return _selectedArticle; }
            set
            {
                _selectedArticle = value;
                OnPropertyChanged("SelectedArticle");
            }
        }

        private bool _isWebViewVisible;
        public bool IsWebViewVisible { get{return _isWebViewVisible;}
            set
            {
                _isWebViewVisible = value;
                OnPropertyChanged("IsWebViewVisible");
            }
        }

        private bool _isProgressRingVisible;
        public bool IsProgressRingVisible {
            get { return _isProgressRingVisible;}
            set
            {
                _isProgressRingVisible = value;
                OnPropertyChanged("IsProgressRingVisible");
            }
        }

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        #endregion

        #region Methods
        public async Task<XDocument> GetRssContent()
        {
            XDocument content = null;
            try
            {
                using (var client = new HttpClient())
                {
                    var request = await client.GetAsync(_rssPath);
                    if (request.IsSuccessStatusCode)
                    {
                        var response = await request.Content.ReadAsStringAsync();
                        content = XDocument.Parse(response);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return content;
        }

        public void GetContentData(XDocument document)
        {
            var posts = (from articles in document.Descendants("item")
                         select new Article
                         {
                             Title = articles.Element("title").Value,
                             Description = RemoveSpecialChars(articles.Element("description").Value),
                             Image = new Uri(ExtractLinks(_imageRegexPath, articles.Element("description").Value)),
                             ReadMore = ExtractLinks(_readMeRegexPath, articles.Element("description").Value)
                         }).ToList();
            Articles = new ObservableCollection<Article>(posts);
        }
        #endregion

        #region Helpers
        private string ExtractLinks(string regex, string description)
        {
            return Regex.Match(description, regex, RegexOptions.IgnoreCase).Groups[1].Value;
        }

        private string RemoveSpecialChars(string response)
        {
            var result = Regex.Replace(response, "<[^>]+>", string.Empty);
            result = System.Net.WebUtility.HtmlDecode(result);
            const string removeString = "Read More";
            result = result.Remove(result.IndexOf(removeString, StringComparison.Ordinal), removeString.Length);
            return result.Replace("\r", "").Replace("\n", "");
        }
        #endregion
    }
}
