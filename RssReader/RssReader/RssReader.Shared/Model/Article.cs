﻿using System;
namespace RssReader.Model
{
    public class Article
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public Uri Image { get; set; }
        public string ReadMore { get; set; }
    }
}
